from app.api.endpoints import execution
from fastapi import APIRouter

api_router = APIRouter()
api_router.include_router(
    execution.router,
    tags=["back_groud_execution"],
)

from app.core.automation import perform_automation
from app.core.log import logger
from app.db.dependency import get_mongodb_client
from app.dependencies.executor_header_validation import executor_header_validation
from app.schemas.executor_schema import PostBodySchema
from fastapi import APIRouter, BackgroundTasks, Depends, Request

router = APIRouter()


@router.post("/run_code")
async def run_code(
    request: Request,  # Include Request in your parameters
    request_data: PostBodySchema,
    background_tasks: BackgroundTasks,
    request_header: str = Depends(executor_header_validation),
    mongodb_client=Depends(get_mongodb_client),
):
    # mongodb_client = request.app.state.mongodb_client
    # You now have validated JSON body in request_data and the custom header in x_custom_header
    # Add perform_automation to background tasks with necessary parameters
    background_tasks.add_task(
        perform_automation, request_data, request_header, mongodb_client
    )

    return {"message": "Task is being processed in the background"}

from app.api.api import api_router
from app.core.log import logger

# from app.db.db_events import register_db_startup_event, register_db_shutdown_event
from app.db.lifetime import init_mongodb, shutdown_mongodb
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


def get_app() -> FastAPI:
    app = FastAPI(title="CDP Project")
    # Add CORS middleware to allow requests from any origin
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )

    # Include your API router
    app.include_router(api_router, prefix="/api")

    @app.on_event("startup")
    async def startup_event():
        # This calls the startup function directly
        await init_mongodb(app)

    @app.on_event("shutdown")
    async def shutdown_event():
        # This calls the shutdown function directly
        await shutdown_mongodb(app)

    return app


app = get_app()

from fastapi import Header, HTTPException


def executor_header_validation(
    coupon: str = Header("1036856", alias="Coupon"),
    user: str = Header("CJFLYAIXPHAHCW3F", alias="User"),
    pw: str = Header("NGSQ", alias="Pw"),
    token: str = Header("rPX-FHchZIpRPT7Pk4OnGQ", alias="Token"),
    domain: str = Header("admazesdemo-team", alias="Domain"),
):
    # Your validation logic here
    return {"Coupon": coupon, "User": user, "Pw": pw, "Token": token, "Domain": domain}

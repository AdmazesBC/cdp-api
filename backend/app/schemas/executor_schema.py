from typing import Any, Dict, Optional

from pydantic import BaseModel, Field, validator


class ExecutionData(BaseModel):
    subscriber_id: Optional[int] = 1762700058173677568
    list_id: Optional[int] = 248543
    event_at: Optional[str] = "2024-03-12 08:21:44.000 UTC"
    event: Optional[str] = "click"
    campaign_id: Optional[int] = 350476


class SeedInfo(BaseModel):
    blockType: Optional[str] = "email_activity_trigger"
    nextTransition: Optional[str] = "NEXT"
    nodeType: Optional[str] = "TRIGGER"
    executionData: Optional[ExecutionData] = Field(default_factory=ExecutionData)


class EventDetails(BaseModel):
    seedInfo: Optional[SeedInfo] = Field(default_factory=SeedInfo)
    event_chain_counter: Optional[int] = 1
    chain_start_time: Optional[int] = 1710231704312


class Contact(BaseModel):
    country: Optional[str] = None
    address: Optional[str] = None
    city: Optional[str] = None
    facebook: Optional[str] = None
    mobile: Optional[str] = None
    last_name: str = "Woodward"
    linkedin: Optional[str] = None
    middle_name: Optional[str] = None
    time_zone: Optional[str] = None
    zipcode: Optional[str] = None
    twitter: Optional[str] = None
    phone: Optional[str] = None
    company: Optional[str] = None
    state: Optional[str] = None
    first_name: str = "Erin"
    email: str = "admazes.demo.customer+58605@gmail.com"


class Data(BaseModel):
    contact: Optional[Contact] = Field(default_factory=Contact)
    event_details: Optional[EventDetails] = Field(default_factory=EventDetails)
    custom_data: Optional[Dict[str, Any]] = {}


class PostBodySchema(BaseModel):
    event_type: str = "contact"
    data: Data = Field(default_factory=Data)
    id: str = "4473fa9a-1dc0-4fbc-9987-66b03f448f8e"
    event: str = "check_email_activity_condition"
    event_category: str = "system"
    event_time: int = 1710231704389

    @validator("data")
    def email_must_not_be_null(cls, v):
        if v.contact.email is None:
            raise ValueError("Email must not be null")
        return v

# backend/app/services/crm.py
import json
from urllib.parse import quote_plus

import httpx
import requests
from app.core.log import logger
from bson.json_util import default, dumps, loads
from httpx import AsyncClient


async def get_crm_contacts_list(domain: str, token: str, data_text: any):
    data = data_text.dict()  # json.loads(data_text)
    print(data)
    postemail = data["data"]["contact"]["email"]
    encoded_email = quote_plus(postemail)

    contacts_url = f"https://{domain}.myfreshworks.com/crm/sales/api/lookup?q={encoded_email}&f=email&entities=contact"
    contacts_headers = {
        "Authorization": f"Token token={token}",
        "Content-Type": "application/json",
    }

    async with httpx.AsyncClient() as client:
        contacts_response = await client.get(contacts_url, headers=contacts_headers)
        contacts_response.raise_for_status()
        contacts_data = contacts_response.json()
        contacts_list = contacts_data.get("contacts", {}).get("contacts", [])
        logger.info("contacts_list collected from CRM")
    return contacts_list, postemail


def update_contact_information(domain, token, coupon, qr_pic):
    id = coupon["id"]
    update_url = f"https://{domain}.myfreshworks.com/crm/sales/api/contacts/{id}"
    update_payload = {"contact": {"custom_field": {"cf_qr_png": qr_pic}}}
    update_headers = {
        "Authorization": f"Token token={token}",
        "Content-Type": "application/json",
    }
    response = requests.put(update_url, headers=update_headers, json=update_payload)
    logger.info(response.content)
    if response.status_code == 200:
        logger.info(f"Updated contact {id} with QR code")
    else:
        logger.info(f"Failed to update contact {id} with QR code")


async def update_mongo_and_crm(
    mongodb_client, domain, token, coupon, response_headers, coupon_url
):
    email_to_check = coupon["email"]
    workflow = response_headers.get("Coupon", "")

    # Assuming you've correctly handled MongoDB operations synchronously or asynchronously elsewhere
    db = mongodb_client["CDP"]
    collection = db["Coupon"]

    # Assuming the MongoDB driver you're using supports async; otherwise, this needs adjustment
    existing_user = await collection.find_one({"user_email": email_to_check})
    if existing_user:
        await collection.update_one(
            {"user_email": email_to_check},
            {"$set": {workflow: coupon_url}},
            upsert=True,
        )
    else:
        await collection.insert_one(
            {"user_email": email_to_check, workflow: coupon_url}
        )
    logger.info("MongoDB updated")

    # Fetch the updated record for CRM update - assuming support for async operations
    updated_user_record = await collection.find_one({"user_email": email_to_check})

    # Convert the MongoDB document to a JSON-compatible Python dict
    json_string = dumps(updated_user_record)
    data_dict = loads(json_string)

    # Prepare the payload for the CRM update
    result_dict = {k: v for k, v in data_dict.items() if k != "user_email"}

    url = f"https://{domain}.myfreshworks.com/crm/marketer/mas/api/v1/events"
    params = {"email": email_to_check, "event_name": "Mezzofy Coupon URL"}
    headers = {
        "Authorization": f"Token token={token}",
        "Content-Type": "application/json",
    }

    # Asynchronous HTTP POST request
    async with AsyncClient() as client:
        response = await client.post(
            url, params=params, headers=headers, json=result_dict
        )

    if response.status_code == 200:
        logger.info(f"Updated CRM for contact with coupon URL: {coupon_url}")
    else:
        logger.error(f"Failed to update CRM for contact. Response: {response.text}")

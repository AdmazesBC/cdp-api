# backend/app/services/mezzofy.py
import base64

import httpx
import requests
from app.core.log import logger
from app.setting import MEZZOFY_ACCOUNT, MEZZOFY_AUTH_URL


async def get_mezzofy_access_token(user: str, pw: str) -> str:
    token_str = f"{user}:{pw}"
    encoded_bytes = base64.b64encode(token_str.encode("utf-8"))
    encoded_string = encoded_bytes.decode("ascii")

    url = MEZZOFY_AUTH_URL
    payload = "grant_type=client_credentials"
    headers = {
        "account": MEZZOFY_ACCOUNT,
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": f"Basic {encoded_string}",
    }

    async with httpx.AsyncClient() as client:
        response = await client.post(url, headers=headers, data=payload)
        response_data = response.json()
        logger.info("mezzofy access token collected")
        return response_data.get("accessToken", "")


def create_coupon_payload(workflow: str, contacts_list: list, postemail: str):
    coupon_payload = []
    for contact in contacts_list:
        crmemail = contact.get("email")
        if crmemail == postemail:
            contact_id = contact.get("id")
            contact_name = contact.get("display_name")
            contact_email = contact.get("email")

            coupon_payload.append(
                {
                    "customer": {
                        "name": contact_name,
                        "email": contact_email,
                        "id": contact_id,
                    },
                    "delivery_method": "E",
                    "lang": "en",
                    "transaction_amount": "10",
                    "coupons": [{"coupon_code": workflow, "coupon_count": "1"}],
                }
            )
    logger.info(f"Post email used in coupon payload creation: {postemail}")
    return coupon_payload


def issue_coupons(access_token, coupon_payload):
    coupon_list = []
    coupon_url = "https://transaction.mzapi.mezzofy.com/v2/issue"
    coupon_headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {access_token}",
    }

    for payload in coupon_payload:
        coupon_response = requests.post(
            coupon_url, headers=coupon_headers, json=payload
        )
        if coupon_response.status_code == 200:
            coupon_data = coupon_response.json()
            transaction_id = coupon_data["transaction_id"]
            email = payload["customer"]["email"]
            user_id = payload["customer"]["id"]
            coupon_list.append(
                {"transaction_id": transaction_id, "email": email, "id": user_id}
            )
            logger.info(f"cupon {transaction_id} issued")
        else:
            logger.info("Failed to issue coupon for contact")
    return coupon_list


def commit_coupon(access_token, coupon):
    transaction_id = coupon["transaction_id"]
    commit_url = (
        f"https://transaction.mzapi.mezzofy.com/v2/issue/{transaction_id}/commit"
    )
    commit_payload = {
        "pay_receipt": "ch_1H16uOKVQiMrqoYIe40AlCSH",
        "payment_name": "Stripe",
        "coupon_delivery_type": "N",
        "purchase_receipt": "N",
    }
    commit_headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {access_token}",
    }

    commit_response = requests.post(
        commit_url, headers=commit_headers, json=commit_payload
    )
    if commit_response.status_code == 200:
        coupon_data = commit_response.json()
        coupon_no = coupon_data["customer_coupons"][0]["customer_coupon"][
            "coupon_serial"
        ]
        logger.info(f"coupon_serial {coupon_no} commited")
        return coupon_no
    else:
        logger.info("Failed to commit coupon")
        return None


def get_coupon_serial(coupon_no, access_token):
    # coupon_no = "42BZLVJU9M"
    coupon_url = f"https://serial.mzapi.mezzofy.com/v2/{coupon_no}"
    payload = {}
    response = requests.request(
        "GET",
        coupon_url,
        headers={"Authorization": f"Bearer {access_token}"},
        data=payload,
    )
    if response.status_code == 200:
        coupon_data = response.json()
        mezzofy_url = coupon_data["couponserial"]["serial"]["single_serial_url"]
        qr_pic = coupon_data["couponserial"]["serial"]["qrcode_url"]
        logger.info(f"get coupon single_serial_url {mezzofy_url}, qrcode_url {qr_pic}")
        return mezzofy_url, qr_pic
    else:
        logger.info("Failed to get coupon serial")
        return None

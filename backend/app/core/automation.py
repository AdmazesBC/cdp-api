# app/core/automation.py
from typing import Any, Dict, Tuple

from app.core.log import logger
from app.services.crm import (
    get_crm_contacts_list,
    update_contact_information,
    update_mongo_and_crm,
)
from app.services.mezzofy import (
    commit_coupon,
    create_coupon_payload,
    get_coupon_serial,
    get_mezzofy_access_token,
    issue_coupons,
)


def _get_workflow_token_domain_user_pw(
    response_headers: Dict[str, Any]
) -> Tuple[str, str, str, str, str]:
    """
    Extracts and returns workflow-related information from response headers.
    Prefixed with an underscore to indicate 'internal use' within the core.automation module.
    """
    workflow = response_headers.get("Coupon", "")
    token = response_headers.get("Token", "")
    domain = response_headers.get("Domain", "")
    user = response_headers.get("User", "")
    pw = response_headers.get("Pw", "")
    return workflow, token, domain, user, pw


async def perform_automation(
    data_text: Any, response_headers: Dict[str, Any], mongodb_client
) -> None:
    # Dummy implementation of an async task
    # Replace with your actual logic
    workflow, token, domain, user, pw = _get_workflow_token_domain_user_pw(
        response_headers=response_headers
    )
    access_token = await get_mezzofy_access_token(user=user, pw=pw)
    contacts_list, postemail = await get_crm_contacts_list(
        domain=domain, token=token, data_text=data_text
    )

    coupon_payload = create_coupon_payload(workflow, contacts_list, postemail)
    coupon_list = issue_coupons(access_token, coupon_payload)

    for coupon in coupon_list:
        coupon_no = commit_coupon(access_token, coupon)
        coupon_url, qr_pic = get_coupon_serial(coupon_no, access_token)

        update_contact_information(domain, token, coupon, qr_pic)

        update_mongo_and_crm(
            mongodb_client, domain, token, coupon, response_headers, coupon_url
        )
    # Assume further async operations here...
    # logger.info(f"Processing data: {data_text} with custom header: {response_headers}")

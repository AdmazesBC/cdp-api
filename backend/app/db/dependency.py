from typing import AsyncGenerator

from pymongo import MongoClient
from starlette.requests import Request


async def get_mongodb_client(
    request: Request,
) -> AsyncGenerator[MongoClient, None]:  # pragma: no cover
    return request.app.state.mongodb_client

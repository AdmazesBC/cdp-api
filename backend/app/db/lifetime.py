import os

from app.core.log import logger
from app.db.utils import check_db_awake
from fastapi import FastAPI
from motor.motor_asyncio import AsyncIOMotorClient


async def init_mongodb(app: FastAPI) -> None:
    """
    Creates connection pool for database.

    :param app: current FastAPI application.
    """
    user = os.getenv("MONGO_DB_USER", "")
    password = os.getenv("MONGO_DB_PASSWORD", "")
    host = os.getenv("MONGO_DB_HOST", "")
    connection_string = (
        f"mongodb+srv://{user}:{password}@{host}/?retryWrites=true&w=majority"
    )
    mongodb_client = AsyncIOMotorClient(connection_string)
    await check_db_awake(
        client=mongodb_client
    )  # Ensure this function is correctly implemented
    logger.info(f"mongodb client initiated ")
    app.state.mongodb_client = mongodb_client


async def shutdown_mongodb(app: FastAPI) -> None:
    """
    Closes database connection pool.

    :param app: current FastAPI app.
    """
    # 'close' is synchronous in Motor
    app.state.mongodb_client.close()
    logger.info(f"mongodb client shutdown ")

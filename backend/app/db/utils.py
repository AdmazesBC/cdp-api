import asyncio

from motor.motor_asyncio import AsyncIOMotorClient


# Function to check if the database is awake by using the 'ping' command
async def check_db_awake(client: AsyncIOMotorClient) -> None:
    try:
        # The 'admin' database is always present and a good target for a ping
        db = client.admin
        # Perform the ping operation
        await db.command("ping")
        print("Database is awake and responsive.")
    except Exception as e:
        print(f"An error occurred: {e}")
        # Depending on your application's needs, you might want to handle reconnection here
        # or raise the exception for the calling code to handle it.
        raise

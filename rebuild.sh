# to re-run the app `chmod +x rebuild.sh; ./rebuild.sh`

# Stop and remove the current running container (if it exists)
docker stop cdp-container
docker rm cdp-container

# Remove the existing image (if it exists)
# docker volume rm cdp-volume

# Run the Docker container with a bind mount from the current ./backend directory
docker run -d \
  --network=bridge \
  --name cdp-container \
  -p 8080:80 \
  --env-file backend/.env \
  -v ./backend:/app \
  cdp_image \
  uvicorn app.main:app --reload --host 0.0.0.0 --port 80
